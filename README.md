Canned project for Tools/Techniques of Sci Comp class

/output contains plots, scripts and data files used to generate them

/src contains code, input file, Makefile, and additional scripts to streamline convergence data output

code built and run on local workstation, instead of stampede. Scripts used to build libraries from tarballs included as well. 
