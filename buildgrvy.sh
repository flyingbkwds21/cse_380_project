#!/bin/sh

export GRVY_VERSION=0.32.0
export BOOST_VERSION=1.65.1
echo $BOOST_DIR

TOPDIR=`pwd`
rm -rf $TOPDIR/grvy-$GRVY_VERSION-src
tar xvzf grvy-$GRVY_VERSION.tar.gz
mv grvy-$GRVY_VERSION grvy-$GRVY_VERSION-src
cd grvy-$GRVY_VERSION-src || exit 1

export CSE_GRVY=/workspace/ypoondla/cse_libraries/grvy
#cd /workspace/ypoondla/0.32.0/ 
./configure --prefix=$CSE_GRVY --with-boost=$CSE_BOOST --without-hdf5 2>&1 | tee configure.log
make -j 4 2>&1 | tee make.log
make check 2>&1 | tee check.log
make install
mv config.log configure.log $CSE_GRVY
mv make.log check.log $CSE_GRVY
cd ..
#rm -rf $TOPDIR/grvy-$GRVY_VERSION-src
