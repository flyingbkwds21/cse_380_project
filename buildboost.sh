#!/bin/sh
set -e # Fail on first error

VERSION=1.65.1
FILENAME=1_65_1

ARCH=$COMPILER-$COMPILER_VERSION

TOPDIR=`pwd`
rm -rf boost-$VERSION-src
if [ -e boost_$FILENAME.tar.bz2 ]; then
  tar xjf boost_$FILENAME.tar.bz2
fi
if [ -e boost_$FILENAME.tar.gz ]; then
  tar -zxf boost_$FILENAME.tar.gz
fi
mv boost_$FILENAME/ boost-$VERSION-src
cd boost-$VERSION-src

./bootstrap.sh \
    --prefix=$TOPDIR/boost-$VERSION-$ARCH \
    --without-libraries=mpi,wave
./bjam -j 3 \
       --build-type=complete \
       --layout=versioned \
install

cd ..
