#!/bin/sh
set -e # Fail on first error

VERSION=2.3
FILENAME=2.3
ARCH=$COMPILER-$COMPILER_VERSION

TOPDIR=`pwd`
rm -rf "$TOPDIR/gsl-$VERSION-$ARCH"
tar xzf gsl-$FILENAME.tar.gz
mv gsl-$FILENAME gsl-$VERSION-src
cd gsl-$VERSION-src || exit 1
./configure --prefix=$TOPDIR/gsl-$VERSION-$ARCH
make -j 1
make -j1 install
cd ..
rm -rf gsl-$VERSION-src
