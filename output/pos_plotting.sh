#!/bin/bash
gnuplot -persist << EOF
  set termopt enhanced
  set object rectangle from screen 0,0 to screen 1,1 behind fillcolor rgb 'white fillstyle solid noborder
  set title "Particle Position, Charged Problem"
  set xlabel "X"
  set ylabel "Y"
  set zlabel "Z"

  splot "part_pos_steps_100000001_freq_10000_method_rk8pd.dat" using 2:3:4 title "Position" with lines
  set term png
  set output "charged_position.png"
  replot
EOF
