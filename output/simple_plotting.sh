#!/bin/bash
gnuplot -persist << EOF
  set termopt enhanced
  set object rectangle from screen 0,0 to screen 1,1 behind fillcolor rgb 'white fillstyle solid noborder
  set title "Error Convergence, Simple Problem: RK4 comparison"
  set xlabel "log_{10}(dt)
  set ylabel "log_{10}(e_{n})
  set logscale xy
  set key top left
  set grid ytics lc rgb "#bbbbbb" lw 1 lt 0
  set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
  plot 'simple_tpl_rk4.dat' using 1:2 title "TPL" with linespoints pointtype 7 pointsize 2, \
       'simple_hand_rk4.dat' using 1:2 title "Hand Coded" with linespoints pointtype 7 pointsize 2, \

  set term pngcairo
  set output "simple_prob_converge.png"
  replot
  



EOF
