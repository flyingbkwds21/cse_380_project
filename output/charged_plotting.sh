#!/bin/bash
gnuplot -persist << EOF
  set termopt enhanced
  set object rectangle from screen 0,0 to screen 1,1 behind fillcolor rgb 'white fillstyle solid noborder
  set title "Error Convergence, Charged Problem: RK comparison"
  set xlabel "log_{10}(dt)
  set ylabel "log_{10}(e_{n})
  set logscale xy
  set key top left
  plot 'charged_tpl_rk8pd.dat' using 1:2 title "RK8PD" with linespoints pointtype 7 pointsize 2, \
       'charged_tpl_rk4.dat' using 1:2 title "RK4" with linespoints pointtype 7 pointsize 2, \
       'charged_tpl_rk2.dat' using 1:2 title "RK(2,3)" with linespoints pointtype 7 pointsize 2, \
       
  set term pngcairo
  set output "charged_prob_converge.png"
  replot
  



EOF
#       'charged_tpl_rk8pd.dat' using 1:2 title "RK8PD" with linespoints pointtype 7 pointsize 3, \


