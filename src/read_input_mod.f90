module Readinput




  implicit none

  public :: read_input


contains

  subroutine read_input()

    use CommandLineProcessor
    
    implicit none

    character(len=128) :: input_filename 

    call get_input_filename( input_filename )

    call read_input_grvy( input_filename )

    return
  end subroutine read_input


  subroutine read_input_grvy( input_filename )
  
    use grvy
    use problem
    use timestepping

    implicit none

    character(len=128), intent(in) :: input_filename
    character(len=128) :: err_output
    integer :: status
    integer :: problem
    double precision :: dt,final_time
    integer :: verif
    integer :: output 
    integer :: method
    integer :: screen_freq
  
      call grvy_timer_begin('read input')
 
      call grvy_input_fopen( trim(adjustl(input_filename)), status )

      call grvy_input_fread_int( "problem", problem, status )
      
      call grvy_input_fread_int( "output", output, status )

      call grvy_input_fread_int( "screen_frequency", screen_freq, status )
    
      call grvy_input_fread_double( "dt", dt, status )

      call grvy_input_fread_double( "final_time", final_time, status )

      call grvy_input_fread_int( "method", method, status )

      call grvy_input_fread_char( "err_filename", err_output, status )


!raw input parameter dump if verbose mode is on
      if(output.eq.verbose) then 
        print *, "problem=",problem
        print *, "method=",method
        print *, "final_time=",final_time
        print *, "dt=",dt
        print *, "screen frequency=",screen_freq
        print *, "output=",output
        print *, "err_filename=",err_output
        call sleep(3) 
        
      endif     

      call set_method( method )

      call set_output( output, screen_freq, err_output )

      call set_problem( problem, final_time, dt )

      call grvy_timer_end('read input')
  end subroutine read_input_grvy

end module Readinput
