#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>



void simple_step( double val_0, double time_0, double dt, int steps, double lambda_in, int method, 
                  int output, int screen_freq )//, char *err_output )

{
//getting convergence data file set by shell script
const char* err_file = getenv("c_err_file");
printf("Convergence Data File: %s\n",(err_file!=NULL)? err_file : "getenv returned NULL, no convergence data file");

//output flags
const int standard = 1;
const int verbose = 2;
const int converge = 3;

//variable defs
double lambda = lambda_in; 
double an_val;
double err;
double t = 0.0;
double y[1] = { val_0 };
double err_norm = 0.0;
int i, s;


//gsl documentation routines to set up ode solver
int
func (double t, const double y[], double f[],
      void *params)
{
  (void)(t); /* avoid unused parameter warning */
  double lambda  = *(double *)params;
  f[0] = lambda * y[0];
  return GSL_SUCCESS;
}
int
jac (double t, const double y[], double *dfdy,
     double dfdt[], void *params)
{
  (void)(t); /* avoid unused parameter warning */
  double lambda  = *(double *)params;
  gsl_matrix_view dfdy_mat
    = gsl_matrix_view_array (dfdy, 1, 1);
  gsl_matrix * m = &dfdy_mat.matrix;
  gsl_matrix_set (m, 0, 0, lambda );
  dfdt[0] = 0.0;
  return GSL_SUCCESS;
}

//initialize ode solver
gsl_odeiv2_system sys = { func, jac, 1, &lambda };

gsl_odeiv2_driver *d =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk4,
                                 dt, 1e-2, 1e-2);

//Timestepping
for (i = 0; i < steps; i++)
  {
    s = gsl_odeiv2_driver_apply_fixed_step (d, &t, dt, 1, y);

//Error Calculations
    an_val = val_0 * exp( lambda * t );

    err = fabs( an_val - y[0] ) / an_val;

    err_norm = err_norm + err;

    if (s != GSL_SUCCESS)
      {
        printf ("error: driver returned %d\n", s);
        break;
      }

//Output Statements 
    if(output==verbose)
    {
      if(((i%(screen_freq*60)) == 0) || (i == 1))
      {
        printf ("   Time (s)    Timestep           Num. Sol.           An. Sol.            Err(%) \n");
      }
  
      if(i==1)
      {
        printf ("%.5e %6s %d %7s %.5e %6s %.5e %5s %.5e\n", t, " ", i, " ", y[0], " ", an_val, " ", (err*100.0));
      }

      if((i%screen_freq)==0)
      {
        printf ("%.5e %6s %d %7s %.5e %6s %.5e %5s %.5e\n", t, " ", i, " ", y[0], " ", an_val, " ", (err*100.0));
      }
    }
    else if(output==standard)
    {
      if(((i%(screen_freq*60)) == 0) || (i == 1))
      {
        printf ("    Time (s)             Sol. \n");
      }
  
      if(i==1)
      {
        printf ("%.5e %5s %.5e \n", t, " ",  y[0]);
      }

      if((i%screen_freq)==0)
      {
        printf ("%.5e %5s %.5e \n", t, " ",  y[0]);
      }
    }
    else if(output==converge)
    {

    }
    else
    {
    
      printf( "Error: wrong choice of output in simple_step.c. Check input file \n");

    }


  }
//Convergence Data Output
  err_norm = err_norm / ((double) steps);
  if((output==converge) && (s==GSL_SUCCESS))
  {
    FILE *errf;
    printf("Writing to file %s \n",err_file);

    errf = fopen(err_file,"a");

    fprintf(errf,"%.9e %5s %.9e \n",dt, " ", err_norm);

    fclose(errf);

    printf ("%.5e %5s %.5e \n", dt, " ", err_norm);

  }
gsl_odeiv2_driver_free (d);

}
