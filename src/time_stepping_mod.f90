module timestepping

  use iso_c_binding, only : c_char, c_null_char, c_int, c_double
  use grvy

  implicit none

  
  private

  integer, parameter :: library = 1
  integer, parameter :: manual = 2

  integer, parameter :: rk2 = 1
  integer, parameter :: rk4 = 2
  integer, parameter :: rk8 = 3

  integer, parameter :: standard = 1
  integer, parameter :: verbose =  2
  integer, parameter :: converge = 3

  integer :: method
  integer :: output
  integer :: screen_freq
  character(len=128) :: err_output


  public :: time_step
  public :: time_step_charged
  public :: set_method
  public :: set_output
  public :: verbose
!interface to call c routines that use gsl library
  interface

    subroutine simple_step( val_0, time_0, dt, steps, lambda, method, output, screen_freq ) bind(c)
      import :: c_int, c_double
      integer(c_int), value :: steps, method, output, screen_freq
      real(c_double), value :: val_0, time_0, dt, lambda
    end subroutine simple_step

    subroutine charged_step( x_0, y_0, z_0, u_0, v_0, w_0, time_0, dt, steps, omega, tau, method, output, screen_freq ) bind(c)
      import :: c_int, c_double
      integer(c_int), value :: steps, method, output, screen_freq
      real(c_double), value :: x_0, y_0, z_0, u_0, v_0, w_0, time_0, dt, omega, tau 

    end subroutine charged_step

  end interface


contains 

  subroutine set_method( method_in )
    
    implicit none

    integer, intent(in) :: method_in

    method = method_in

  end subroutine set_method
  
  subroutine set_output( output_in, screen_freq_in, err_output_in )

    implicit none
    integer, intent(in) :: output_in
    integer, intent(in) :: screen_freq_in
    character(len=128), intent(in) :: err_output_in

    output = output_in
    screen_freq = screen_freq_in
    err_output = err_output_in 

  end subroutine set_output

    
  subroutine time_step( val_0, time_0, dt, steps, lambda )

    implicit none

    integer, intent(in) :: steps
    double precision, intent(in) :: dt
    double precision, intent(in) :: time_0
    double precision, intent(in) :: val_0
    double precision, intent(in) :: lambda

    double precision :: val, time, k1, k2, k3, k4, err, an_val, err_norm
    integer :: i

    val = val_0

    time = time_0
    
    err_norm = 0d0


    select case( method )

    case( library )
!extra words for verbose
      call grvy_timer_begin('tpl simple')

      if(output.eq.verbose) then
        print *, 'running with tpl'
        call sleep(1) 
      endif

      call simple_step( val_0, time_0, dt, steps, lambda, method, output, screen_freq )
      
      call grvy_timer_end('tpl simple')

    case( manual )

      call grvy_timer_begin('hand simple')
!extra words for verbose
      if(output.eq.verbose) then 
        print *, 'running with with handcoded rk4'
        call sleep(1) 
      endif
!hand-written RK4
      do i = 1,steps
        time = time + dt
        k1 = dt * lambda * val
        k2 = dt * lambda * (val + (0.5d0 * k1)) 
        k3 = dt * lambda * (val + (0.5d0 * k2))
        k4 = dt * lambda * (val + k3)

        val = val + (k1/6d0) + (k2/3d0) + (k3/3d0) + (k4/6d0)

        an_val = val_0 * exp( lambda * time )

        err = abs( an_val - val ) / an_val 
      
        err_norm = err_norm + err

!screen output
        select case( output )

        case( standard )

          if(modulo(i,screen_freq*60).eq.0.or.i.eq.1) print *, "     Time(s)    ",&
          "          Sol."

          if( i.eq.1 ) print 1, time, val
          if( modulo( i,screen_freq ).eq.0 ) print 1, time, val
          if( i.eq.steps ) print 1, time, val


1         format(' ', F12.6, E18.6)

        case( verbose )

          if(modulo(i,screen_freq*60).eq.0.or.i.eq.1) print *, "     Time(s)    ",&
          "   Timestep         Num. Sol.          An. Sol.            Err "

          if( i.eq.1 ) print 2, time, i, val, an_val, (err)

          if( modulo( i,screen_freq ).eq.0 ) print 2, time, i, val, an_val, (err)

          if( i.eq.steps ) print 2, time, i, val, an_val, (err)

2         format(' ', F12.6, I15, 3E18.6)

        case( converge )

        case default
          print *, "Error: incorrect choice of screen output. Check input file"
          stop
        end select

      enddo
    
      err_norm = err_norm / dble(steps)

      
!Convergence data output
      if( output.eq.converge ) then 
        print *, dt, err_norm

        open(unit=60,file=trim(adjustl(err_output)),status='old',position='append')
        write(60,*) dt, err_norm
        close(60)
      endif
      call grvy_timer_end('hand simple')

    case default 
      print *, "Error: incorrect method choice for simple problem. Check input file"
      stop
    end select


  end subroutine time_step

  subroutine time_step_charged( x_0, y_0, z_0, u_0, v_0, w_0, time_0, dt, steps, omega, tau )

    implicit none

    integer, intent(in) :: steps
    double precision, intent(in) :: dt
    double precision, intent(in) :: time_0
    double precision, intent(in) :: x_0, y_0, z_0, u_0, v_0, w_0
    double precision, intent(in) :: omega, tau

    call grvy_timer_begin('charged')

    call charged_step( x_0, y_0, z_0, u_0, v_0, w_0, time_0, dt, steps, omega, tau, &
                       method, output, screen_freq )

    call grvy_timer_end('charged')

  end subroutine time_step_charged


end module timestepping
