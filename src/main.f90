

program main 

	use grvy
  use Readinput
  use CommandLineProcessor
  use problem

	implicit none
  integer :: status
!add use commands to call on grvy and gsl

!	print *, "Hello"
  call grvy_timer_init('Initializing timer')

  call process_command_line_args()

  call read_input()

  call run_problem()

  call grvy_timer_finalize()

  call grvy_timer_summarize()

end

