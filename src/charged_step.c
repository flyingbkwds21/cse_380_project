#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <unistd.h>
#include <stddef.h>


void charged_step( double x_0, double y_0, double z_0, double u_0, double v_0, double w_0, double time_0, 
                  double dt, int steps, double omega_in, double tau_in, int method, int output, int screen_freq )//, char *err_output )

{
//define struct needed to pass ode params
struct param_type
{
  double tau;
  double omega;
};

//get convergence data file from environrment variable set by shell script
const char* err_file = getenv("c_err_file");
printf("Convergence Data File: %s\n",(err_file!=NULL)? err_file : "getenv returned NULL, no convergence data file");

//output and method flags
const int standard = 1;
const int verbose = 2;
const int converge = 3;

const int rk2 = 1;     
const int rk4 = 2;
const int rk8 = 3;

//variable defs
double omega = omega_in;
double tau = tau_in;

double t = 0.0;
double y[6] = { x_0, y_0, z_0, u_0, v_0, w_0 };
double err[6];
double an_y[6];
double temp1[6],temp2[6];

double err_norm = 0.0;
int i, s;
double omega2 = pow(omega,2.0);
double tau2 = pow(tau,2.0);

char meth_str[5];

FILE *posf;

struct param_type charge_params = {tau, omega};

//gsl documentation routines to set up ode solver
int
func (double t, const double y[], double f[],
      void *params)
{
  (void)(t); /* avoid unused parameter warning */
  struct param_type *charge_params_pointer = params;

  f[0] = y[3];
  f[1] = y[4];
  f[2] = y[5];
  f[3] = omega*y[4] - (y[3]/tau);
  f[4] = -omega*y[3] - (y[4]/tau);
  f[5] = -y[5]/tau; //should be y[5] going off of font, and symmetry. we'll find out, I guess
  return GSL_SUCCESS;
}
int
jac (double t, const double y[], double *dfdy,
     double dfdt[], void *params)
{
  (void)(t); /* avoid unused parameter warning */
  struct param_type *charge_params_pointer = params;

  gsl_matrix_view dfdy_mat
    = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix * m = &dfdy_mat.matrix;
  gsl_matrix_set_all(m, 0.0);

  gsl_matrix_set (m, 0, 3, 1.0  );
  gsl_matrix_set (m, 1, 4, 1.0  );
  gsl_matrix_set (m, 2, 5, 1.0  );
  gsl_matrix_set (m, 3, 3, -1/tau );
  gsl_matrix_set (m, 3, 4, omega );
  gsl_matrix_set (m, 4, 3, -omega );
  gsl_matrix_set (m, 4, 4, -1/tau );
  gsl_matrix_set (m, 4, 4, 1.0 );

  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  dfdt[2] = 0.0;
  dfdt[3] = 0.0;
  dfdt[4] = 0.0;
  dfdt[5] = 0.0;

  return GSL_SUCCESS;
}


//initialize ode solver
gsl_odeiv2_system sys = { func, jac, 6, &charge_params };


//initialize 3 drivers
//if statement in loop to choose which one
//unwieldy, but it works
//
gsl_odeiv2_driver *rk2_d =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk2, dt, 1e-2, 1e-2);

gsl_odeiv2_driver *rk4_d =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk4, dt, 1e-2, 1e-2);

gsl_odeiv2_driver *rk8_d =
  gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, dt, 1e-2, 1e-2);

//setting up position file stuff
if(method == rk2)
{

  printf("Step type left as rk2\n");
  sprintf(meth_str,"rk2");

}
else if(method == rk4)
{
  printf("Step type set to rk4\n");
  sprintf(meth_str,"rk4");

}
else if(method == rk8)
{
  printf("Step type set to rk8pd\n");
  sprintf(meth_str,"rk8pd");

}

//sleep(2);

//Setting up position file, only written for a normal run
if(output!=converge)
{
  char pos_file[50];
  printf("%s \n","writing particle position file");
  sprintf(pos_file,"../output/part_pos_steps_%d_freq_%d_method_%s.dat",steps,screen_freq,meth_str);
  posf = fopen(pos_file ,"w");
  fprintf(posf,"#Charged Particle Position Data\n");
  fprintf(posf,"#time  x  y  z\n");

  fprintf(posf,"%.5e %.9e %.9e %.9e\n",t, y[0], y[1], y[2]);
}
//timestepping
for (i = 0; i < steps; i++)
  {
//unwieldy if statement here
    if(method == rk2)
    {
     s = gsl_odeiv2_driver_apply_fixed_step(rk2_d, &t, dt, 1, y);
    
    }
    else if(method == rk4)
    {
     s = gsl_odeiv2_driver_apply_fixed_step(rk4_d, &t, dt, 1, y);
   
    }
    else if(method == rk8)
    {
     s = gsl_odeiv2_driver_apply_fixed_step(rk8_d, &t, dt, 1, y);
  
    }

//analytic solution and error calculation
    an_y[3] = 20.0 * exp( -t / tau ) * cos( omega * t);
    an_y[4] = -20.0 * exp( -t / tau ) * sin( omega * t);
    an_y[5] = 2.0 * exp( -t / tau );

    an_y[0] =( ( 20.0 * tau * exp( -t / tau ) *  ( ( tau * omega  * sin( t * omega ) ) - cos( t * omega ) ) ) /
              ( ( omega2 * tau2 ) + 1.0 ) ) + ( 50.0 / 313.0 );
    an_y[1] =( ( 20.0 * tau * exp( -t / tau ) *  ( ( tau * omega  * cos( t * omega ) ) + sin( t * omega ) ) ) /
              ( ( omega2 * tau2 ) + 1.0 ) ) - ( 1250.0 / 313.0 );

    an_y[2] = (2.0 * tau) * ( 1.0 - exp( -t / tau ) );

    err[0] = fabs( an_y[0] - y[0]) / an_y[0];  
    err[1] = fabs( an_y[1] - y[1]) / an_y[1];  
    err[2] = fabs( an_y[2] - y[2]) / an_y[2];  
    err[3] = fabs( an_y[3] - y[3]) / an_y[3];  
    err[4] = fabs( an_y[4] - y[4]) / an_y[4];  
    err[5] = fabs( an_y[5] - y[5]) / an_y[5];  

//calculate norm of all 6 errors and sum up. Average outside of time loop
    err_norm = err_norm + sqrt( pow(err[0],2.0) + pow(err[1],2.0) + pow(err[2],2.0) + pow(err[3],2.0) + pow(err[4],2.0) + pow(err[5],2.0) );       

    if (s != GSL_SUCCESS)
      {
        printf ("error: driver returned %d\n", s);
        break;
      }

//screen and file output
    if(output==verbose)
    {
      if(((i%(screen_freq*60)) == 0) || (i == 0))
      {
        printf ("%3s %s %13s %s %13s %s %12s %s %12s %s %12s %s %12s %s \n",
        " ","Time(s)"," ","ErrX"," ","ErrY"," ","ErrZ"," ","ErrU"," ","ErrV"," ","ErrW");
      }
  
      if(i==0)
      {
        printf ("%.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e \n", 
                  t, " ",  err[0], " ",  err[1], " ",  err[2], " ",  err[3], " ",  err[4], " ",  err[5]);
        fprintf(posf,"%.5e %.9e %.9e %.9e\n",t, y[0], y[1], y[2]);
      }

      if((i%screen_freq)==0)
      {
        printf ("%.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e \n", 
                  t, " ",  err[0], " ",  err[1], " ",  err[2], " ",  err[3], " ",  err[4], " ",  err[5]);
        fprintf(posf,"%.5e %.9e %.9e %.9e\n",t, y[0], y[1], y[2]);

      }
    }
    else if(output==standard)
    {
      if(((i%(screen_freq*60)) == 0) || (i == 0))
      {
        printf ("%3s %s %16s %s %16s %s %15s %s %15s %s %15s %s %15s %s \n"," ","Time(s)"," ","X"," ","Y"," ","Z"," ","U"," ","V"," ","W");
      }
  
      if(i==0)
      {
        printf ("%.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e \n", 
                  t, " ",  y[0], " ",  y[1], " ",  y[2], " ",  y[3], " ",  y[4], " ",  y[5]);
        fprintf(posf,"%.5e %.9e %.9e %.9e\n",t, y[0], y[1], y[2]);

      }

      if((i%screen_freq)==0)
      {
        printf ("%.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e %5s %.5e \n", 
                  t, " ",  y[0], " ",  y[1], " ",  y[2], " ",  y[3], " ",  y[4], " ",  y[5]);
        fprintf(posf,"%.5e %.9e %.9e %.9e\n",t, y[0], y[1], y[2]);

      }
    }
    else if(output==converge)
    {

    }
    else
    {
    
      printf( "Error: wrong choice of output in simple_step.c. Check input file \n");

    }


  }
//close position file
  if(output!=converge)
  {
    fclose(posf);
  }

//calculate average of norm
  err_norm = err_norm / ((double) steps);

//write error data only if solver completes
//can fail if error exceeds set tolerance of 1e-2
  if((output==converge) && (s==GSL_SUCCESS))
  {
    FILE *errf;
    printf("Writing to file %s \n",err_file);

    errf = fopen(err_file,"a");

    fprintf(errf,"%.9e %5s %.9e \n",dt, " ", err_norm);

    fclose(errf);

    printf ("%.5e %5s %.5e \n", dt, " ", err_norm);

  }

  if(method == rk2)
  {
    gsl_odeiv2_driver_free (rk2_d);
  
  }
  else if(method == rk4)
  {
    gsl_odeiv2_driver_free (rk4_d);

 
  }
  else if(method == rk8)
  {
    gsl_odeiv2_driver_free (rk8_d);


  }



}
