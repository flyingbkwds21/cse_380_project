module problem

  use timestepping

  implicit none

  private

  integer, parameter :: simple = 1
  integer, parameter :: charged = 2


  integer :: problem_type
  double precision :: final_time
  double precision :: dt
  integer :: steps

  public :: set_problem
  public :: run_problem

  

  
contains

  subroutine set_problem( problem_type_in, final_time_in, dt_in )

    implicit none

    integer, intent(in) :: problem_type_in
    double precision, intent(in) :: final_time_in, dt_in

    problem_type = problem_type_in
    final_time = final_time_in
    dt = dt_in
    steps = int(final_time/dt) + 1

    return
  end subroutine set_problem


  subroutine run_problem()

    implicit none


    select case( problem_type )

    case( simple )

      call simple_problem()

    case( charged )

      call charged_problem()

    case default

      print *, "Error, invalid problem type parameter. Check input file"
      stop
    end select
  end subroutine run_problem
  
  
  subroutine simple_problem()

    implicit none

    double precision :: val, time
    double precision, parameter :: lambda = -1.0d0


    val = 5.0d0
    time = 0.0d0


    call time_step( val, time, dt, steps, lambda )

  end subroutine simple_problem

    
  subroutine charged_problem()
  
    implicit none

    double precision :: x, y, z, u, v, w, time
    double precision, parameter :: omega = 5.0d0
    double precision, parameter :: tau = 5.0d0
  
    x = 0.0d0
    y = 0.0d0
    z = 0.0d0

    u = 20.0d0
    v = 0.0d0
    w = 2.0d0

    time = 0.0d0

    call time_step_charged( x, y, z, u, v, w, time, dt, steps, omega, tau )    
    
  end subroutine charged_problem 

end module problem     
