#!/bin/bash
make clean
make

#file='../output/simple_tpl_rk4.dat'
#sed -i "s/method = .*/method = 1/" proj.inp
#echo "#Convergence Data: Simple problem, tpl rk4" > $file

file='../output/simple_hand_rk4.dat'
sed -i "s/method = .*/method = 2/" proj.inp
echo "#Convergence Data: Simple problem, hand coded rk4" > $file

export c_err_file=$file
#echo $c_err_file

sed -i "s+err_filename = .*+err_filename = $file+" proj.inp
sed -i "s/output = .*/output = 3/" proj.inp
sed -i "s/problem = .*/problem = 1/" proj.inp

echo "#dt  err" >> $file
dt_sci='1e-7'
#echo $dt_sci
#dt_start=`echo ${dt_sci} | sed -e 's/[eE]+*/
dt_start=${dt_sci/[eE]/*10^}
#echo $dt_start
dt_mult='1.5'

for i in  $(seq 1 1 40) ;
do
dt=`echo "($dt_start * ($dt_mult^($i - 1)))" | bc -l`
#echo "$dt"
sed -i "s/dt = .*/dt = $dt/" proj.inp


./main proj.inp
#sed -n '13p' proj.inp
#sed -n '63p' proj.inp

done



